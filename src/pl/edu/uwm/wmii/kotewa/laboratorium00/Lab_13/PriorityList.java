package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_13;

import java.util.PriorityQueue;
import java.util.Scanner;

public class PriorityList {
    public PriorityQueue<Task> tasks;

    public PriorityList(){
        this.tasks = new PriorityQueue<>();
    }

    public void UserInput(){
        Scanner sc = new Scanner(System.in);
        String[] input;

        String polecenie = sc.nextLine();
        while (!polecenie.equals("zakoncz")){
            input = polecenie.split(" ");
            if(input[0].equals("dodaj") && input.length == 3){
                int priority = Integer.parseInt(input[1]);
                String desc = input[2];
                tasks.add(new Task(priority, desc));
                System.out.print("Dodano nowe zadanie\n");
            } else if (input[0].equals("nastepne")) {
                tasks.poll();
                assert tasks.peek() != null;
                System.out.printf("Usunieto zadanie: (%d, %s)\n", tasks.peek().priority, tasks.peek().desc);
                System.out.printf("Pozostało %d zadanie/a\n", tasks.size());
            } else {
                System.out.print("Error: nieprawidłowa komenda\n");
            }
            polecenie = sc.nextLine();
        }
        System.out.print("Zakonczono");
    }

    public static void main(String[] args) {
        PriorityList priorityList = new PriorityList();
        priorityList.UserInput();

    }
}

class Task implements Comparable<Task>{
    int priority;
    String desc;

    public Task(int priority, String desc){
        this.priority = priority;
        this.desc = desc;
    }

    @Override
    public int compareTo(Task a){
        return Integer.compare(this.priority, priority);
    }
}
