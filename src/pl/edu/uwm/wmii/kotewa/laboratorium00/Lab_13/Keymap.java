package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_13;

import java.util.Map.Entry;
import java.util.TreeMap;

public class Keymap {

    TreeMap<Students, String> grades;
    TreeMap<Integer, Students> IDs;

    public Keymap(){
        this.grades = new TreeMap<>();
        this.IDs = new TreeMap<>();
    }

    public void Dodaj(String first, String last, int ID, String grade){
        Students n_student = new Students(first, last, ID);
        this.grades.put(n_student, grade);
        this.IDs.put(ID, n_student);
    }

    public void Usun(int ID){
        this.grades.remove(IDs.get(ID));
    }

    public void Zmien(int ID, String grade){
        this.grades.replace(IDs.get(ID), grade);
    }

    public void Wypisz(){
        for (Entry<Students, String> entry : grades.entrySet()) {
            System.out.printf("[ %d, %s, %s, " + entry.getValue() + "]\n", entry.getKey().ID, entry.getKey().first, entry.getKey().last);
        }
    }

    public static void main(String[] args) {
        Keymap egz = new Keymap();
        egz.Dodaj("Jakub", "Gornicz", 157216, "bdb");
        egz.Dodaj("Roman", "Budkowski", 353421, "dst+");
        egz.Dodaj("AZofia", "Kozłowska", 112427, "db-");
        egz.Dodaj("Zofia", "Kozłowska", 112427, "db-");
        egz.Wypisz();
    }
}
