package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_13;

public class Students implements Comparable<Students>{
    String first;
    String last;
    int ID;

    public Students(String first, String last, int ID){
        this.first = first;
        this.last = last;
        this.ID = ID;
    }

    @Override
    public int compareTo(Students stu){
        if (this.last.compareTo(stu.last)<=0){
            if(this.first.compareTo(stu.first) <= 0){
                if(this.ID > stu.ID){
                    return 1;
                } else if (this.ID == stu.ID){
                    return 0;
                } else{
                    return -1;
                }
            } else {
                return this.first.compareTo(stu.first);
            }
        } else {
            return this.last.compareTo(stu.last);
        }
    }
}