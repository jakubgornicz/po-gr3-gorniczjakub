package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie4.pl.imiajd.gornicz;

import pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie4.pl.imiajd.gornicz.Osoba;

public class Student extends Osoba {

    private String kierunek;
    public Student(String nazwisko, int rokUrodzenia, String kierunek){
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }

    public String get_kierunek(){
        return kierunek;
    }

    public void to_String(){
        System.out.printf("%s\n", kierunek);
    }
}
