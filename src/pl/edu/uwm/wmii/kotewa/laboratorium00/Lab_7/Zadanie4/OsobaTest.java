package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie4;

import pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie4.pl.imiajd.gornicz.Osoba;
import pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie4.pl.imiajd.gornicz.Nauczyciel;
import pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie4.pl.imiajd.gornicz.Student;

public class OsobaTest {
    public static void main(String[] args) {
        Osoba[] ludzie = new Osoba[2];
        ludzie[0] = new Nauczyciel("Nowak", 1961,50000);
        ludzie[1] = new Student("Kowalski", 1999, "informatyka");

        for (Osoba p : ludzie) {
            System.out.println(p.get_nazwisko());
            System.out.println(p.get_rokUrodzenia());
            p.to_String();
            System.out.println("\n");
        }
    }
}
