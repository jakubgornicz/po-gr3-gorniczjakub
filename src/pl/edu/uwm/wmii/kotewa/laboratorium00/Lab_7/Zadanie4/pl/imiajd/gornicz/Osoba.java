package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie4.pl.imiajd.gornicz;

public class Osoba {
    private String nazwisko;
    private int rokUrodzenia;

    public Osoba(String nazwisko, int rokUrodzenia){
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }

    public String get_nazwisko(){
        return nazwisko;
    }

    public int get_rokUrodzenia(){
        return rokUrodzenia;
    }

    public void to_String() {
        System.out.printf("%s %d\n", nazwisko, rokUrodzenia);
    }
}
