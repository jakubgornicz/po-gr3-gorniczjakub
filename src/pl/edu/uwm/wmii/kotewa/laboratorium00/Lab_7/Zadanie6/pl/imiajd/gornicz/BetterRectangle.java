package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie6.pl.imiajd.gornicz;

import java.awt.Rectangle;


public class BetterRectangle extends Rectangle {

    public BetterRectangle(int width, int height, int x, int y){
        super(width, height, x, y);
        setSize(width, height);
        setLocation(x, y);
    }

    public double getParameter(){
        return Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2));
    }

    public int GetArea(){
        return width * height;
    }

}
