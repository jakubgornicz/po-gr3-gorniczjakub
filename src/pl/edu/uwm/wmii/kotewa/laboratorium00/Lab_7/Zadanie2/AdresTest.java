package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie2;

import pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie2.pl.imiajd.gornicz.Adres;

public class AdresTest {
    public static void main(String[] args) {
        Adres test1 = new Adres("10-089", "Olsztyn", "Iwaszkiewicza", 15, 3);
        Adres test2 = new Adres("10-084", "Olsztyn", "Iwaszkiewicza", 15);
        test1.pokaz();

        System.out.println(test1.przed(test2));
    }

}
