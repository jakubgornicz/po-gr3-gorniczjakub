package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie6;


import pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie6.pl.imiajd.gornicz.BetterRectangle;

public class BetterRectangleTest {
    public static void main(String[] args) {

        BetterRectangle r1 = new BetterRectangle(4, 4, 0, 0);
        BetterRectangle r2 = new BetterRectangle(3, 4, 0, 0);

        System.out.printf("parameter of r2: %.3f\n", r1.getParameter());
        System.out.printf("area of r2: %d\n", r1.GetArea());

        System.out.println();

        System.out.printf("parameter of r2: %.3f\n", r2.getParameter());
        System.out.printf("area of r2: %d\n", r2.GetArea());
    }
}
