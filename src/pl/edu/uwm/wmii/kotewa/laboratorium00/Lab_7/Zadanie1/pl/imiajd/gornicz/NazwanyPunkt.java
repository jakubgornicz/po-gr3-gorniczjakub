package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie1.pl.imiajd.gornicz;

import pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie1.pl.imiajd.gornicz.Punkt;

public class NazwanyPunkt extends Punkt{

    public NazwanyPunkt(int x, int y, String name)
    {
            super(x, y);
            this.name = name;
    }

    public void show()
    {
            System.out.println(name + ":<" + x() + ", " + y() + ">");
    }

    private String name;

}
