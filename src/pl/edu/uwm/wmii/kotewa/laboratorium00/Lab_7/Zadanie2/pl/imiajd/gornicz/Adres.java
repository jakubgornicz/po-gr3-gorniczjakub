package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie2.pl.imiajd.gornicz;

public class Adres {
    private String ulica;
    private String miasto;
    private String kod_pocztowy;
    private int numer_domu;
    private int numer_mieszkania;

    public Adres(String kod_pocztowy, String miasto, String ulica, int numer_domu){
        this.kod_pocztowy = kod_pocztowy;
        this.miasto = miasto;
        this.ulica = ulica;
        this.numer_domu = numer_domu;
    }

    public Adres(String kod_pocztowy, String miasto, String ulica, int numer_domu, int numer_mieszkania){
        this.kod_pocztowy = kod_pocztowy;
        this.miasto = miasto;
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
    }

    public void pokaz(){
        System.out.printf("%s %s%n",kod_pocztowy, miasto);
        System.out.printf("%s %d/%d%n",ulica, numer_domu, numer_mieszkania);
    }

    public boolean przed(Adres other){
        int compare = kod_pocztowy.compareTo(other.kod_pocztowy);
        if(compare < 0) {return true;}
        else {return false;}

    }
}
