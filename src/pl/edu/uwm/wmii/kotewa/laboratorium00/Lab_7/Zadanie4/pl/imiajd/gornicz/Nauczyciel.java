package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie4.pl.imiajd.gornicz;

import pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_7.Zadanie4.pl.imiajd.gornicz.Osoba;

public class Nauczyciel extends Osoba {

    private int pensja;
    public Nauczyciel(String nazwisko, int rokUrodzenia, int pensja){
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }

    public int get_pensja(){
        return pensja;
    }
    public void to_String(){
        System.out.printf("%d\n", pensja);
    }
}