package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_3;

import java.util.Random;

public class Lab3_Zadanie2_g {
    public static void generuj (int[] tab, int n, int minWartosc, int maxWartosc){
        Random random = new Random();
        for(int i=0; i<n; i++) {
            tab[i] = random.nextInt((maxWartosc-minWartosc)+1) + minWartosc;
        }
    }
    public static void wypisz(int[] tab, int n){
        for(int i=0; i<n; i++) {
            System.out.println(tab[i]);
        }
    }
    public static void odwrocFragment(int[] tab, int lewy, int prawy){
        int temp1;
        if(lewy>prawy){
            temp1 = lewy;
            lewy = prawy;
            prawy = temp1;
        }
        int temp2;
        int pomoc=0;
        for(int j=lewy; j<((double)prawy+(double)lewy)/2; j++){
            temp2 = tab[j];
            tab[j] = tab[prawy-pomoc];
            tab[prawy-pomoc] = temp2;
            pomoc += 1;
        }
    }
    public static void main(String[] args) {

        // creating n
        Random random = new Random();
        int n;
        int max_n = 100;
        int min_n = 1;
        n = random.nextInt((max_n - min_n) + 1) + min_n;

        int[] array = new int[n];
        int max = 999;
        int min = -999;

        int lewy = random.nextInt(((n-1) - min_n)+1) + min_n;
        int prawy = random.nextInt(((n-1) - min_n)+1) + min_n;
        generuj(array,n,min,max);
        wypisz(array,n);
        System.out.println("------------------------------");
        System.out.println("size of array: "+n);
        System.out.println("altered fragment from index: "+lewy+" to: "+prawy);
        odwrocFragment(array, lewy, prawy);
        wypisz(array,n);


    }
}


