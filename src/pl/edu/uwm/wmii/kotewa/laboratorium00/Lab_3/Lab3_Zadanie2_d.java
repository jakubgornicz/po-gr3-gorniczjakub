package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_3;

import java.util.Random;

public class Lab3_Zadanie2_d {
    public static void generuj (int[] tab, int n, int minWartosc, int maxWartosc){
        Random random = new Random();
        for(int i=0; i<n; i++) {
            tab[i] = random.nextInt((maxWartosc-minWartosc)+1) + minWartosc;
        }
    }
    public static void wypisz(int[] tab, int n){
        for(int i=0; i<n; i++) {
            System.out.println(tab[i]);
        }
    }
    public static int sumaDodatnich(int[] tab){
        int sum_of_positive = 0;
        for (int j : tab) {
            if(j>0) sum_of_positive += j;
        }
        return sum_of_positive;
    }
    public static int sumaUjemnych(int[] tab){
        int sum_of_negative = 0;
        for (int j : tab) {
            if(j<0) sum_of_negative += j;
        }
        return sum_of_negative;
    }
    public static void main(String[] args) {

        // creating n
        Random random = new Random();
        int n;
        int max_n = 100;
        int min_n = 1;
        n = random.nextInt((max_n - min_n) + 1) + min_n;

        int[] array = new int[n];
        int max = 999;
        int min = -999;
        generuj(array,n,min,max);
        wypisz(array,n);
        System.out.println("sum of positive elements: "+sumaDodatnich(array));
        System.out.println("sum of positive elements: "+sumaUjemnych(array));

    }
}
