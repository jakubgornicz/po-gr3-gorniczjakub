package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_3;

import java.util.Random;

public class Lab3_Zadanie1_b {
    public static void main(String[] args) {

        // creating n
        Random random = new Random();
        int n;
        int max_n = 100;
        int min_n = 1;
        n = random.nextInt((max_n - min_n)+1) + min_n;

        int[] array = new int[n];
        //filling array, counting odd and even and zeros
        int max = 999;
        int min = -999;
        int  even_count = 0;
        int odd_count = 0;
        int zeros = 0;
        for(int i=0; i<n; i++) {
            array[i] = random.nextInt((max-min)+1) + min;
            //System.out.println(array[i]);
            if (array[i]%2==0) even_count += 1;
            else if (array[i]==0) zeros += 1;
            else  odd_count += 1;

        }

        System.out.println("size of array: "+n);
        System.out.println("even numbers in array: "+even_count);
        System.out.println("zeros in array: "+zeros);
        System.out.println("odd numbers in array: "+odd_count);



    }
}
