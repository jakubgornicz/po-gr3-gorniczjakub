package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_3;

import java.util.Random;



public class Lab3_Zadanie1_g {
    public static void main(String[] args) {

        // creating n
        Random random = new Random();
        int n;
        int max_n = 100;
        int min_n = 1;
        n = random.nextInt((max_n - min_n)+1) + min_n;

        int[] array = new int[n];
        //filling array and counting sum of positive elements and separately sum of negative elements
        int max = 999;
        int min = -999;


        for(int i=0; i<n; i++) {
            array[i] = random.nextInt((max-min)+1) + min;
        }
        for(int i=0; i<n; i++) System.out.println(array[i]);
        System.out.println("----------------------");


        int lewy = random.nextInt(((n-1) - min_n)+1) + min_n;
        int prawy = random.nextInt(((n-1) - min_n)+1) + min_n;

        int temp1;
        if(lewy>prawy){
            temp1 = lewy;
            lewy = prawy;
            prawy = temp1;
        }
        int temp2;
        int pomoc=0;
        for(int j=lewy; j<((double)prawy+(double)lewy)/2; j++){
            temp2 = array[j];
            array[j] = array[prawy-pomoc];
            array[prawy-pomoc] = temp2;
            pomoc += 1;
        }


        System.out.println("size of array: "+n);
        System.out.println("altered fragment from index: "+lewy+" to: "+prawy);
        for(int i=0; i<n; i++) System.out.println(array[i]);




    }
}
