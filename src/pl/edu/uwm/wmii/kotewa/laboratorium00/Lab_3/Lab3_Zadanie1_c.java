package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_3;

import java.util.Random;

public class Lab3_Zadanie1_c {
    public static void main(String[] args) {

        // creating n
        Random random = new Random();
        int n;
        int max_n = 100;
        int min_n = 1;
        n = random.nextInt((max_n - min_n)+1) + min_n;

        int[] array = new int[n];
        //filling array and counting instances of max value
        int max = 999;
        int min = -999;

        int max_value = -999;
        int max_value_instances = 0;
        for(int i=0; i<n; i++) {
            array[i] = random.nextInt((max-min)+1) + min;
            //System.out.println(array[i]);
            if(array[i]>max_value){
                max_value = array[i];
                max_value_instances += 1;
            }

        }

        System.out.println("size of array: "+n);
        System.out.println("max value in array: "+max_value);
        System.out.println("instances of max value in array: "+max_value_instances);



    }
}
