package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_8.Zadanie3.pl.imiajd.gornicz;

import java.time.LocalDate;

public abstract class Instrument {
    String producent;
    LocalDate rokProdukcji;

    public Instrument(String producent, LocalDate rokProdukcji){
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    public String getProducent(){
        return producent;
    }

    public LocalDate getRokProdukcji(){
        return rokProdukcji;
    }

    public abstract String dzwiek();

    public String toString(){
        return producent.toString() + rokProdukcji.toString();
    }


    public boolean equals(Instrument obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        return true;
    }


}
