package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_8.Zadanie3.pl.imiajd.gornicz;

import java.time.LocalDate;

public class Fortepian extends Instrument{
    public Fortepian(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }

    @Override
    public String dzwiek() {
        return "dang";
    }
}
