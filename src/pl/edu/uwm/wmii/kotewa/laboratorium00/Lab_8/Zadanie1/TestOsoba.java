package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_8.Zadanie1;

import java.time.LocalDate;
import pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_8.Zadanie1.pl.imiajd.gornicz.*;

public class TestOsoba {
    public static void main(String[] args)
    {
        LocalDate date1 = LocalDate.of(1960, 3, 13);
        LocalDate date2 = LocalDate.of(1999, 4, 24);
        LocalDate date3 = LocalDate.of(1980, 3, 13);

        Osoba[] ludzie = new Osoba[2];
        ludzie[0] = new Pracownik("Kowalski", date1, true, 50000, date2);
        ludzie[1] = new Student("Nowak", date3, false, "informatyka", 3.5);

        Student s2 = new Student("Kowalska", date3, false, "informatyka", 0.0);

        System.out.println("before set_SredniaOcen: " + s2.get_SredniaOcen());
        s2.set_SredniaOcen(4.0);
        System.out.println("after set_SredniaOcen: " + s2.get_SredniaOcen());
        System.out.println();

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
            System.out.println("plec: " + p.get_plec() + "\ndata urodzenia: " + p.get_dataUrodzenia());
            System.out.println("\n");
        }

    }

}




