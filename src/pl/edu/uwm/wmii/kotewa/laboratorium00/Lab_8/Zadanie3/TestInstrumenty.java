package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_8.Zadanie3;

import pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_8.Zadanie1.pl.imiajd.gornicz.Osoba;
import pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_8.Zadanie3.pl.imiajd.gornicz.*;

import java.time.LocalDate;

public class TestInstrumenty {
    public static void main(String[] args){

        Instrument[] instruments = new Instrument[5];
        instruments[0] = new Fortepian("Yamaha", LocalDate.of(1980, 3, 13));
        instruments[1] = new Flet("Stag", LocalDate.of(1991, 1, 12));
        instruments[2] = new Skrzypce("Stromberg", LocalDate.of(1970, 11, 16));
        instruments[3] = new Skrzypce("Stromberg", LocalDate.of(1961, 12, 20));
        instruments[4] = new Skrzypce("Stromberg", LocalDate.of(1972, 4, 20));

        for(Instrument i : instruments) {
            System.out.printf("%s %s\n",i.getProducent(), i.getRokProdukcji());
            System.out.println(i.dzwiek());
            System.out.println();
        }

        Instrument i1 = new Fortepian("Yamaha", LocalDate.of(1980, 3, 13));
        Instrument i2 =  new Flet("Wamaha", LocalDate.of(1981, 3, 23));
        Instrument i3 =  new Fortepian("Yamaha", LocalDate.of(1980, 3, 13));
        System.out.println(i1.equals(i2) + "\n" + i1.equals(i3));
    }
}
