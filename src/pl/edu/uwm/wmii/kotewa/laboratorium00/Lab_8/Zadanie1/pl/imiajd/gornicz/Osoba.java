package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_8.Zadanie1.pl.imiajd.gornicz;

import java.time.LocalDate;

public abstract class Osoba {

    public Osoba(String nazwisko, LocalDate dataUrodzenia, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }

    public String[] get_imiona()
    {
        return imiona;
    }

    public LocalDate get_dataUrodzenia()
    {
        return dataUrodzenia;
    }

    public String get_plec()
    {
        if(plec) {
            return "mezczyzna";
        }else return "kobieta";


    }
    public String nazwisko;
    public String[] imiona;
    public java.time.LocalDate dataUrodzenia;
    public boolean plec;
}
