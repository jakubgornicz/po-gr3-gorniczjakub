package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_8.Zadanie3.pl.imiajd.gornicz;

import java.time.LocalDate;

public class Skrzypce extends Instrument{
    public Skrzypce(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }

    @Override
    public String dzwiek() {
        return "iiiiiiiiiiiiiii";
    }
}
