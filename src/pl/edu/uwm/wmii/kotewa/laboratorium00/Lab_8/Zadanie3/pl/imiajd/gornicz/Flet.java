package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_8.Zadanie3.pl.imiajd.gornicz;

import java.time.LocalDate;

public class Flet extends Instrument{
    public Flet(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }

    @Override
    public String dzwiek() {
        return "fuuuuuuuuuuuu";
    }
}
