package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_8.Zadanie1.pl.imiajd.gornicz;

import java.time.LocalDate;


public class Student extends Osoba {
    public Student(String nazwisko, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis()
    {
        return "\nkierunek studiów - " + kierunek + "\nsrednia ocen: " + get_SredniaOcen() ;
    }

    public double get_SredniaOcen()
    {
        return sredniaOcen;
    }

    public void set_SredniaOcen(double new_sredniaOcen)
    {
        sredniaOcen = new_sredniaOcen;
    }

    private String kierunek;
    private double sredniaOcen;
}