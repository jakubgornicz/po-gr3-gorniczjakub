package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_8.Zadanie1.pl.imiajd.gornicz;

import java.time.LocalDate;

public class Pracownik extends Osoba {
    public Pracownik(String nazwisko, LocalDate dataUrodzenia, boolean plec, double pobory, LocalDate dataZatrudnienia)
    {
        super(nazwisko, dataUrodzenia, plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }
    public LocalDate get_dataZatrudnienia()
    {
        return dataZatrudnienia;
    }

    public String getOpis()
    {
        return "\npracownik z pensja: " + getPobory() + "\ndata zatrudnienia: " + get_dataZatrudnienia();
    }

    private double pobory;
    private java.time.LocalDate dataZatrudnienia;
}