package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_11.Zadanie2;

public class PairDemo {
    public static void main(String[] args) {
        Pair<Integer> test = new Pair<>();
        test.setFirst(12);
        test.setSecond(21);
        System.out.printf("Before: first = %d, second = %d\n", test.getFirst(), test.getSecond());
        test.swap();
        System.out.printf("After: first = %d, second = %d\n", test.getFirst(), test.getSecond());

    }
}
