package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_5;

import java.util.ArrayList;

public class Zadanie5 {
    public static void reverse(ArrayList<Integer> L) {
        int temp;
        for(int i = L.size()-1; i >= 0; i--)
            L.add(L.get(i));
        for(int j = 0; j < L.size(); j++)
            L.remove(L.get(j));
    }
    public static void main(String[] args) {
        ArrayList<Integer> arrL = new ArrayList<Integer>(4);
        arrL.add(4);
        arrL.add(3);
        arrL.add(2);
        arrL.add(1);
        System.out.println("Before fun 'reverse': "+arrL);
        reverse(arrL);
        System.out.print("After: "+arrL);


    }
}
