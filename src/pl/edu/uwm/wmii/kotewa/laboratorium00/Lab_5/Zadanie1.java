package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_5;

import java.util.ArrayList;

public class Zadanie1 {
    public static ArrayList<Integer> append(ArrayList<Integer> arrL1, ArrayList<Integer> arrL2){
        ArrayList<Integer> new_arrL = new ArrayList<Integer>();
        new_arrL.addAll(0, arrL1);
        new_arrL.addAll(arrL1.size(), arrL2);
        return new_arrL;
    }
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<Integer>(4);
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);

        ArrayList<Integer> b = new ArrayList<Integer>(4);
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);

        System.out.println(append(a,b));
    }
}
