package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_5;

import java.util.ArrayList;

public class Zadanie4 {
    public static ArrayList<Integer> reversed(ArrayList<Integer> L) {
        ArrayList<Integer> new_arrL = new ArrayList<Integer>();
        for(int i = L.size()-1, j = 0; i >= 0; i--, j++){
            new_arrL.add(j, L.get(i));
        }
        return new_arrL;
    }
    public static void main(String[] args) {
        ArrayList<Integer> arrL = new ArrayList<Integer>(4);
        arrL.add(4);
        arrL.add(3);
        arrL.add(2);
        arrL.add(1);
        System.out.println("Before fun 'reversed': "+arrL);
        System.out.print("After: "+reversed(arrL));


    }
}
