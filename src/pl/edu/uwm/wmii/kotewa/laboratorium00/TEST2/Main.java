package pl.edu.uwm.wmii.kotewa.laboratorium00.TEST2;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static pl.edu.uwm.wmii.kotewa.laboratorium00.TEST2.Kandydet.Qualified;
import static pl.edu.uwm.wmii.kotewa.laboratorium00.TEST2.Kandydet.RecruitmentMap;

public class Main {

    public static void main(String[] args) {
        ArrayList<Kandydet> grupa = new ArrayList<>();
        grupa.add(new Kandydet("Jakub Gornicz", 20, "licencjat", 1));
        grupa.add(new Kandydet("Franz Kafka", 25, "licencjat", 2));
        grupa.add(new Kandydet("Alex Castelanos", 27, "mistrzowie", 5));
        grupa.add(new Kandydet("James Hetfield", 27, "mistrzowie", 6));

        System.out.print("\nBefore:\n");
        for (Kandydet e : grupa) {
            System.out.printf("%s %d, %s, %d\n", e.getNazwa(), e.getWiek(), e.getWyksztalcony(), e.getLatadoswiadczenia());
        }

        Collections.sort(grupa);

        System.out.print("\nAfter:\n");
        for (Kandydet e : grupa) {
            System.out.printf("%s %d, %s, %d\n", e.getNazwa(), e.getWiek(), e.getWyksztalcony(), e.getLatadoswiadczenia());
        }

        Rekrutacja.setDoswiadczenie();

        System.out.print("\nWho qualified:\n");
        for (Kandydet e : grupa) {
            System.out.println(e.getNazwa() + " " + e.getLatadoswiadczenia() + " : " + Qualified(e));
        }

        System.out.print("\nMap of qualified candidates:\n");
        HashMap<Integer, String> map = RecruitmentMap(grupa);
        for (Integer e : map.keySet()) {
            System.out.println(e + " " + map.get(e));
        }

    }


}