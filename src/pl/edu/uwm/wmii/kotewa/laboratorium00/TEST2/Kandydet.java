package pl.edu.uwm.wmii.kotewa.laboratorium00.TEST2;

import java.util.ArrayList;
import java.util.HashMap;


class Kandydet implements Cloneable, Comparable<Kandydet> {
    private String nazwa;
    private int wiek;
    private String wyksztalcony;
    private int latadoswiadczenia;

    public Kandydet(String nazwa, int wiek, String wyksztalcony, int latadoswiadczenia) {
        this.nazwa = nazwa;
        this.wiek = wiek;
        this.wyksztalcony = wyksztalcony;
        this.latadoswiadczenia = latadoswiadczenia;
    }

    @Override
    public int compareTo(Kandydet other) {

        if (this.wyksztalcony.compareTo(other.wyksztalcony) != 0) {
            return this.wyksztalcony.compareTo(other.wyksztalcony);
        }

        if (this.wiek != other.wiek) {

            if (this.wiek < other.wiek)
                return -1;
            else
                return 1;

        }

        if (this.latadoswiadczenia < other.latadoswiadczenia)
            return -1;

        if (this.latadoswiadczenia > other.latadoswiadczenia)
            return 1;

        return 0;
    }

    public String getNazwa() {

        return nazwa;
    }

    public int getLatadoswiadczenia() {

        return latadoswiadczenia;
    }

    public int getWiek() {

        return wiek;
    }

    public String getWyksztalcony() {

        return wyksztalcony;
    }

    public static boolean Qualified(Kandydet k) {
        if(k.getLatadoswiadczenia() >= Rekrutacja.doswiadczenie) {
            return true;
        }
        return false;
    }

    public static HashMap<Integer, String> RecruitmentMap(ArrayList<Kandydet> klist) {
        HashMap<Integer, String>  map = new HashMap<>();
        for(Kandydet e : klist)
            if(Qualified(e))
                map.put(e.getLatadoswiadczenia(), e.getNazwa());
        return map;
    }

}
