package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_6.Zadanie1;

public class RachunekBankowyTest {
    public static void main(String[] args) {
        RachunekBankowy saver1 = new RachunekBankowy(2000.00);
        RachunekBankowy saver2 = new RachunekBankowy(3000.00);
        saver1.setRocznaStopaProcentowa(0.04);

        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("saldo saver1 po pierwszym miesiącu z roczna stopa procentowa " + saver1.get_rocznaStopaProcentowa() + " wynosi: " + saver1.get_saldo());
        System.out.println("saldo saver2 po pierwszym miesiącu z roczna stopa procentowa " + saver1.get_rocznaStopaProcentowa() + " wynosi: " + saver2.get_saldo());
        System.out.println();


        saver1.setRocznaStopaProcentowa(0.05);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("saldo saver1 po kolejnym miesiącu z roczna stopa procentowa " + saver1.get_rocznaStopaProcentowa() + " wynosi: " + saver1.get_saldo());
        System.out.println("saldo saver2 po kolejnym miesiącu z roczna stopa procentowa " + saver1.get_rocznaStopaProcentowa() + " wynosi: " + saver2.get_saldo());
    }
}
