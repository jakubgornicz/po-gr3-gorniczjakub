package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_6.Zadanie1;

public class RachunekBankowy {
    static double rocznaStopaProcentowa = 0.0;
    private double saldo;

    public RachunekBankowy(double saldo){

        this.saldo = saldo;

    }

    public double get_saldo(){
        return saldo;
    }

    public double get_rocznaStopaProcentowa(){
        return rocznaStopaProcentowa;
    }

    public void setRocznaStopaProcentowa(double value){
        rocznaStopaProcentowa = value;
    }

    public void obliczMiesieczneOdsetki(){
        this.saldo = this.saldo + (this.saldo*rocznaStopaProcentowa)/12;
    }


}
