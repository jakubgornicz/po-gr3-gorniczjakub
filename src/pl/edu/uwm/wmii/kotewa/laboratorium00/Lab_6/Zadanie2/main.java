package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_6.Zadanie2;

import java.util.Arrays;

public class main {
    public static void main(String[] args) {
        IntegerSet X = new IntegerSet();
        IntegerSet Y = new IntegerSet();
        IntegerSet M = new IntegerSet();
        IntegerSet N = new IntegerSet();

        X.insertElement(1);
        X.insertElement(3);
        X.insertElement(10);
        X.insertElement(25);

        Y.insertElement(2);
        Y.insertElement(10);
        Y.insertElement(25);

        IntegerSet.union(X, Y).show_BooleanArray();
        IntegerSet.intersection(X, Y).show_BooleanArray();

        X.to_String();
        X.deleteElement(1);
        X.to_String();

        System.out.println();

        Y.to_String();

        System.out.println();

        System.out.println(X.equals(Y));
        System.out.println(M.equals(N));
    }
}
