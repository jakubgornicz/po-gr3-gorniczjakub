package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_6.Zadanie2;
import java.util.Arrays;

public class IntegerSet {
    Boolean[] booleanArray = new Boolean[100];

    public IntegerSet() {
        Arrays.fill(booleanArray, Boolean.FALSE);
    }

    public void show_BooleanArray() {
        for (int i = 0, j = 0; i < 100; i++, j++) {
            if (i == 0) {
                System.out.print("[" + booleanArray[i] + ", ");
                continue;
            }
            if (i == 100 - 1) {
                System.out.print(booleanArray[i] + "]\n\n");
                continue;
            }

            if (j % 10 == 0)
                System.out.print("\n");
            System.out.print(booleanArray[i] + ", ");


        }
    }

    public static IntegerSet union(IntegerSet A, IntegerSet B){
        IntegerSet suma = new IntegerSet();
        for(int i = 0; i < 100; i++){
            suma.booleanArray[i] = A.booleanArray[i] || B.booleanArray[i];
        }
        return suma;
    }

    public static IntegerSet intersection(IntegerSet A, IntegerSet B){
        IntegerSet iloczyn = new IntegerSet();
        for(int i = 0; i < 100; i++){
            iloczyn.booleanArray[i] = A.booleanArray[i] && B.booleanArray[i];
        }
        return iloczyn;
    }

    public void insertElement(int k){
        for(int i = 0; i < 100; i++){
            if(i == k-1) booleanArray[i] = true;
        }
    }

    public void deleteElement(int k){
        for(int i = 0; i < 100; i++){
            if(i == k-1) booleanArray[i] = false;
        }
    }

    public void to_String(){
        String values = "";
        for(int i = 0; i < 100; i++){
            if(booleanArray[i]){
                values += (i+1) + " ";
            }
        }
        System.out.println(values);
    }
    public boolean equals(IntegerSet B){
        for(int i = 0; i < 100; i++){
            if(booleanArray[i] != B.booleanArray[i]) return false;
        }
        return true;
    }
}
