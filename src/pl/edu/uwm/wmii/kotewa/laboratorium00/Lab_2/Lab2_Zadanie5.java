package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_2;
//Wczytac liczbe naturalna n, a nastepnie wczytujac kolejno n liczb rzeczywistych obliczyc ile jest
//wsród nich liczb dodatnich, liczb ujemnych oraz ile jest zer.

import java.util.Arrays;

public class Lab2_Zadanie5 {

    static void count_number_of(int x, int[] y){
        int positive = 0;
        int negative = 0;
        int zeros = 0;
        for(int i=0;i<x;i++){
            if(y[i]>0){
                positive += 1;
            } else if(y[i]<0){
                negative += 1;
            } else zeros += 1;
        }
        System.out.format("number of positive %d%n",positive);
        System.out.format("number of negative %d%n",negative);
        System.out.format("number of zeros %d%n",zeros);
    }
    public static void main(String[] args) {
        int n = 7;
        int[] numbers = {0,-11,2,3,-4,10,15};
        System.out.print(Arrays.toString(numbers)+"\n");
        count_number_of(n, numbers);

    }
}
