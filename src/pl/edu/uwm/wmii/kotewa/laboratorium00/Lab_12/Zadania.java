package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_12;
import java.util.LinkedList;
import java.util.Stack;
import java.util.Iterator;
public class Zadania {
    public static <T> void redukuj(LinkedList<T> list, int n){
        for(int i=n-1; i<list.size();i+=n-1){
            list.remove(i);
        }
    }

    public static <T> void odwroc(LinkedList<T> list){
        LinkedList<T> temp= new LinkedList<>(list);

        for (int i = 0; i < list.size(); i++){
            temp.set(i, list.get(list.size()-1-i));
        }
        for (int i = 0; i < list.size(); i++){
            list.set(i, temp.get(i));
        }
    }

    public static String przeksztalc(String tekst){
        String[] parts= tekst.split(" ");
        Stack<String> result= new Stack<>();
        StringBuilder rev= new StringBuilder();

        for(String part: parts){
            result.push(part);

            if(part.endsWith(".")){
                while(!result.empty()){
                    StringBuilder revpart = new StringBuilder();
                    revpart.append(result.pop());
                    if(result.empty()){
                        revpart.setCharAt(0,Character.toLowerCase(revpart.charAt(0)));
                        rev.append(revpart);
                        rev.append(". ");
                    }
                    else if(revpart.toString().equals(part)){
                        revpart.setCharAt(0,Character.toUpperCase(revpart.charAt(0)));
                        rev.append(revpart, 0, revpart.length()-1);
                        rev.append(" ");
                    }
                    else{
                        rev.append(revpart);
                        rev.append(" ");
                    }
                }
            }
        }
        return rev.toString();
    }

    public static void rozdzielenie(int number){
        Stack<Integer> parts = new Stack<>();

        while(number != 0){
            parts.push(number%10);
            number = number/10;
        }

        while (!parts.empty()){
            System.out.print(parts.pop()+" ");
        }
    }

    public static void Eratostenes(int n){
        LinkedList<Integer> primes = new LinkedList<>();
        for (int i = 2; i <= n; i++){
            primes.add(i);
        }
        for(int j = 0; j < primes.size(); j++){
            for (int k = 0; k < primes.size(); k++){
                if (primes.get(k) % primes.get(j) == 0 && !primes.get(k).equals(primes.get(j))){
                    primes.remove(k);
                }
            }
        }
        System.out.print(primes);
    }

    public static <T extends Iterable<?>> void print(T toIter){
        Iterator<?> res= toIter.iterator();
        while (res.hasNext()){
            System.out.print(res.next());
            if (res.hasNext()){
                System.out.print(", ");
            }
        }
        System.out.println();
    }

    public static void main(String[] args) {
        LinkedList<String> tab = new LinkedList<>();
        tab.add("Jakub");
        tab.add("Andrzej");
        tab.add("Robert");
        tab.add("Michal");
        tab.add("Maks");
        tab.add("Adrian");
        System.out.print(tab + "\n");
        redukuj(tab, 2);
        System.out.print(tab + "\n");
        odwroc(tab);
        System.out.print(tab + "\n");
        System.out.print(przeksztalc("Ala ma kota. Jej kot lubi myszy.") + "\n");
        rozdzielenie(2015);
        System.out.print("\n");
        Eratostenes(40);
    }
}
