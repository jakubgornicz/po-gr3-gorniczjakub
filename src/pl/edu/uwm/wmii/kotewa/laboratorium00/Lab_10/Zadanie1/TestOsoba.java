package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_10.Zadanie1;

import pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_10.Zadanie1.pl.imiajd.gornicz.Osoba;
import pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_10.Zadanie1.pl.imiajd.gornicz.Student;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class TestOsoba {
    public static void main(String[] args) {
        ArrayList<Osoba> grupa = new ArrayList<>();
        grupa.add(new Osoba("Zieliński", LocalDate.of(1998, 2, 26)));
        grupa.add(new Osoba("Nowak", LocalDate.of(1998, 2, 26)));
        grupa.add(new Osoba("Przybyła", LocalDate.of(1998, 2, 26)));
        grupa.add(new Osoba("Górnicz", LocalDate.of(2000, 11, 7)));
        grupa.add(new Osoba("Zieliński", LocalDate.of(1977, 12, 12)));

        System.out.println("Before sorting {");
        for (Osoba p: grupa){
            System.out.print(p.toString() + "\n");
        }

        System.out.println("}\n After{ ");
        Collections.sort(grupa);

        for (int i = 0; i < 5; i++){
            System.out.print(grupa.get(i) + "\n");
        }
        System.out.println("}");

        ArrayList<Student> students = new ArrayList<Student>();
        students.add(new Student("Zieliński", 1998, 7, 26, 3.0));
        students.add(new Student("Nowak", 1998, 5, 13, 4.0));
        students.add(new Student("Przybyła", 1998, 1, 11, 4.5));
        students.add(new Student("Górnicz", 2000, 11, 7, 3.5));
        students.add(new Student("Zieliński", 1998, 4, 26, 2.5));

        System.out.print("Before:\n");
        for (int i = 0; i < 5; i++){
            System.out.print(students.get(i) + "\n");
        }
        Collections.sort(students);
        System.out.print("After:\n");
        for (int i = 0; i < 5; i++){
            System.out.print(students.get(i) + "\n");
        }

        System.out.print("Reading the file:\n");
        ArrayList<String> lines = new ArrayList<String>();
        try {
            File plik = new File(args[0]);
            Scanner sc = new Scanner(plik);
            while (sc.hasNextLine()) {
                lines.add(sc.nextLine());
            }
            sc.close();
        } catch (FileNotFoundException e) {
            System.out.println("no file");
            e.printStackTrace();
        }

        for (String line : lines) {
            System.out.print(line + "\n");
        }

        Collections.sort(lines);

        for (String line : lines) {
            System.out.print(line + "\n");
        }
    }

}
