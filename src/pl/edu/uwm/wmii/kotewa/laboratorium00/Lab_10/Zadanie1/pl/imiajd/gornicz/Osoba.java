package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_10.Zadanie1.pl.imiajd.gornicz;

import java.time.LocalDate;

public class Osoba implements Cloneable, Comparable<Osoba>
{
    public Osoba(String nazwisko, int year, int month, int day)
    {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = LocalDate.of(year, month, day);
    }

    public Osoba(String nazwisko, LocalDate data)
    {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = data;
    }

    @Override
    public String toString()
    {
        return "[nazwisko: " + nazwisko + ", data urodzenia: " + dataUrodzenia + "]";
    }

    public boolean equals(Osoba other)
    {
        return nazwisko.equals(other.nazwisko) && dataUrodzenia.equals(other.dataUrodzenia);
    }

    @Override
    public int compareTo(Osoba o) {
        int result = this.nazwisko.compareTo(o.nazwisko);
        if (result == 0)
            return this.dataUrodzenia.compareTo(o.dataUrodzenia);
        return result;
    }

    public String getNazwisko()
    {
        return nazwisko;
    }

    public LocalDate getDataUrodzenia()
    {
        return dataUrodzenia;
    }

    private String nazwisko;
    private LocalDate dataUrodzenia;
}
