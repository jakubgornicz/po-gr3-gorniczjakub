package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_10.Zadanie1.pl.imiajd.gornicz;

public class Student extends Osoba implements Cloneable, Comparable<Osoba>{

    public Student(String nazwisko, int year, int month, int day, double sredniaOcen) {
        super(nazwisko, year, month, day);
        this.sredniaOcen = sredniaOcen;
    }

    public Student(Osoba o) {
        super(o.getNazwisko(), o.getDataUrodzenia());
        this.sredniaOcen = 0;
    }

    public String toString()
    {
        return "[nazwisko: " + getNazwisko() + ", data urodzenia: " + getDataUrodzenia() + ", srednia:" + sredniaOcen + "]";
    }

    @Override
    public int compareTo(Osoba o) {
        int result = super.compareTo(o);
        if (result == 0){
            return Double.compare(sredniaOcen, ((Student)o).sredniaOcen);
        }
        return result;
    }

    private double sredniaOcen;
}
