package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_4;

import java.io.File;  // Import the File class
import java.util.Scanner;

public class Zadanie2 {
        static int count_chars(String path, char character) throws Exception{
            File given_file = new File(path);
            Scanner myReader = new Scanner(given_file);
            int counter = 0;
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                for (int i = 0; i < data.length(); i++){
                    if (data.charAt(i) == character){
                        counter++;
                    }
                }
            }
            return counter;
        }

        public static void main(String[] args) {
            int char_num = 0;
            String path = "C:\\Users\\zajecia_java\\PO-gr3-GorniczJakub\\src\\pl\\edu\\uwm\\wmii\\kotewa\\laboratorium00\\Lab_week4\\plik.txt";
            try {
                char_num = count_chars(path, 'N');
            } catch (Exception a){
                System.out.println("no file at given path");
            }

            System.out.println("number of chosen character: "+char_num);
        }
}

