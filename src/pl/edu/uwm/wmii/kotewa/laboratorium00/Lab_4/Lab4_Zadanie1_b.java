package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_4;

public class Lab4_Zadanie1_b {
    static int countSubStr(String str, String subStr) {
        int count = 0, fromIndex = 0;
        while ((fromIndex = str.indexOf(subStr, fromIndex)) != -1 ){
            System.out.println("Found at index: "+fromIndex);
            count++;
            fromIndex++;
        }
        return count;
    }
    public static void main(String[] args) {
        String str = "abbabaab";
        String subStr = "ab";
        System.out.println("Phrase '"+subStr+"'in string '"+str+"' has been found: "+countSubStr(str, subStr)+" times.");
    }
}
