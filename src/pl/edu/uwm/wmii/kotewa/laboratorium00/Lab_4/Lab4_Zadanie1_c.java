package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_4;

public class Lab4_Zadanie1_c {
    public static String middle(String str){
        String result;
        if(str.length()%2==0){
            double index = (str.length()-1)/2.0;
            int bound1 = (int) Math.floor(index);
            int bound2 = bound1 + 2;
            result = str.substring(bound1,bound2);
        }else{
            int bound1 = (str.length()-1)/2;
            int bound2 = bound1 + 1;
            result = str.substring(bound1,bound2);
        }
        return result;
    }
    public static void main(String[] args) {
        String str1 = "middle";
        String str2 = "honey";
        System.out.println("character/s located at the middle in string '"+str1+"' is: "+middle(str1));
        System.out.println("character/s located at the middle in string '"+str2+"' is: "+middle(str2));
    }
}
