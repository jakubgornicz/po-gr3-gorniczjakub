package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_4;
import java.io.File;
import java.util.Scanner;

public class Zadanie3 {
        static int count_substring(String path, String subStr) throws Exception{
            File given_file = new File(path);
            Scanner myReader = new Scanner(given_file);
            int counter = 0;
            int word_num = 0;
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                for (int i = 0; i < data.length(); i++){
                    if (data.charAt(i) == subStr.charAt(counter)){
                        counter++;
                        if (counter == subStr.length()){
                            word_num++;
                            counter = 0;
                        }
                    } else {
                        counter = 0;
                    }
                }
            }
            return word_num;
        }

        public static void main(String[] args) {
            int word_num = 0;
            String path = "C:\\Users\\zajecia_java\\PO-gr3-GorniczJakub\\src\\pl\\edu\\uwm\\wmii\\kotewa\\laboratorium00\\Lab_week4\\plik.txt";
            try {
                word_num = count_substring(path, "program");
            } catch (Exception b){
                System.out.println("no file at given path");
            }

            System.out.println("number of chosen words: "+word_num);
        }
}

