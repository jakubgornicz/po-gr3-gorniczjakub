package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_4;
import java.math.RoundingMode;
import java.math.BigDecimal;

public class Zadanie5 {
    static BigDecimal asset_value(int k, double p, int n){
        BigDecimal total = BigDecimal.valueOf(k);
        for (int i = 0; i < n; i++){
            total = total.multiply(BigDecimal.valueOf(1+(p/1)));
        }
        total = total.setScale(2, RoundingMode.CEILING);
        return total;
    }

    public static void main(String[] args) {
        System.out.print(asset_value(2400,0.05,10));
    }
}
