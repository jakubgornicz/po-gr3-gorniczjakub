package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_4;

import java.util.Arrays;

public class Lab4_Zadanie1_e {
    public static int[] append(int[] array, int value) {
        int[] result = Arrays.copyOf(array, array.length + 1);
        result[result.length - 1] = value;
        return result;
    }
    static int[] where(String str, String subStr) {
        int fromIndex = 0;
        int[] indexes;
        indexes = new int[0];
        while ((fromIndex = str.indexOf(subStr, fromIndex)) != -1 ){
            indexes = append(indexes,fromIndex);
            fromIndex++;
        }
        return indexes;
    }
    public static void main(String[] args) {
        String str = "baabbabaab";
        String subStr = "ab";
        System.out.print(Arrays.toString(where(str, subStr)));
    }
}
