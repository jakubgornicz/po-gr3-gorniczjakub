package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_4;

public class Lab4_Zadanie1_a {
    static int countChar(String str, char c) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != c) {
                continue;
            }
            count += 1;
        }
        return count;
    }
    public static void main(String[] args) {
        String str = "abbabaab";
        char c = 'b';
        System.out.println("ilosc wystapien znaku '"+c+"' w napisie '"+str+"' wynosi: "+countChar(str, c));
    }
}
