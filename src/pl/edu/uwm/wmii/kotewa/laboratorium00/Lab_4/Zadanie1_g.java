package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_4;


public class Zadanie1_g {
    static String nice(String str){
        StringBuffer s = new StringBuffer();
        StringBuffer reversed_str = new StringBuffer(str);
        reversed_str.reverse();
        int counter = 0;
        for (int i = 0; i < str.length(); i++){

            if(counter % 3 == 0){
                counter = 0;
                s.append('\'');
            }
            s.append(reversed_str.charAt(i));
            counter++;
        }
        s.reverse();
        return s.toString();
    }

    public static void main (String[] args){
            String ex = "1234567899";
            System.out.println(nice(ex));


    }

}
// 12'345
// '123
// 1'234
// '123'456