package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_4;

public class Zadanie1_h {
    static String nice(String str, char character, int position) {
        StringBuffer s = new StringBuffer();
        StringBuffer reversed_str = new StringBuffer(str);
        reversed_str.reverse();
        int counter = 0;
        for (int i = 0; i < str.length(); i++) {
            if (counter % position == 0) {
                counter = 0;
                s.append(character);
            }
            s.append(reversed_str.charAt(i));
            counter++;
        }
        s.reverse();

        return s.toString();
    }
    public static void main (String[] args){
        String ex = "12345678997745679";
        System.out.println(nice(ex, '#', 2));


    }
}