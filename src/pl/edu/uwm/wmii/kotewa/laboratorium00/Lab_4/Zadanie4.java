package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_4;
import java.math.BigInteger;

public class Zadanie4 {
    static BigInteger poppy_seeds(int n){
        BigInteger[][] tab = new BigInteger[n][n];
        BigInteger counter = BigInteger.valueOf(1);
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                tab[i][j] = counter;
                counter = counter.multiply(BigInteger.valueOf(2));
            }
        }
        BigInteger total = BigInteger.valueOf(0);
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++)
            {
                total = total.add(tab[i][j]);
            }
        }
        return total;
    }
    public static void main(String[] args) {
        System.out.println(poppy_seeds(7));
    }
}
