package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab_4;

public class Lab4_Zadanie1_d {
    public static String repeat(String str, int n){
        String new_string = "";
        for(int i=0; i<n; i++){
            new_string += str;
        }
        return new_string;
    }
    public static void main(String[] args) {
        String str = "ha";
        int n = 10;
        System.out.println("phrase '"+str+"' repeated "+n+" times is: "+repeat(str, n));

    }
}
